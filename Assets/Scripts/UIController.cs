using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{

    private int contador = 0;
    
    public void accion(string dato)
    {
        Debug.Log($"He pulsado {dato} {contador++} veces...");
    }
}
