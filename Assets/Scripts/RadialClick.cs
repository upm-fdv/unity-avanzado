using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RadialClick : MonoBehaviour
{

    [Header("Timers")]
    public float indicatorTimer = 1.0f;
    public float maxIndicatorTimer = 1.0f;

    [Header("UI")]
    public Image radialIndicatorUI = null;
    
    [Header("Key Codes")]
    public KeyCode key = KeyCode.Mouse1;

    [Header("Events")]
    public UnityEvent clickEvent = null;


    private bool animacion = false;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(key))
        {
            indicatorTimer -= Time.deltaTime;
            radialIndicatorUI.enabled = true;
            radialIndicatorUI.fillAmount = indicatorTimer;

            if (indicatorTimer <= 0)
            {
                indicatorTimer = maxIndicatorTimer;
                radialIndicatorUI.fillAmount = maxIndicatorTimer;
                radialIndicatorUI.enabled = false;
                clickEvent.Invoke();
            }
        }
        else
        {
            if (animacion)
            {
                indicatorTimer += Time.deltaTime;
                radialIndicatorUI.fillAmount = indicatorTimer;

                if (indicatorTimer >= maxIndicatorTimer)
                {
                    indicatorTimer = maxIndicatorTimer;
                    radialIndicatorUI.fillAmount = maxIndicatorTimer;
                    radialIndicatorUI.enabled = false;
                    animacion = false;
                }
            }
        }

        if (Input.GetKeyUp(key))
            animacion = true;
    }
}
