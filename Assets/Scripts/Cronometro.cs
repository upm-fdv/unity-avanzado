using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using TMPro;
using UnityEngine;

public class Cronometro : MonoBehaviour
{
    
    //Declaración de variables
    public GameObject textoTiempo;
    public float tiempoTotal = 0;

    private TextMeshProUGUI timer;
    
    private void Start()
    {
        timer = textoTiempo.GetComponent<TextMeshProUGUI>();
        StartCoroutine(runTimer());
    }

    IEnumerator runTimer()
    {
        while (true)
        {
            //Incremento el tiempo transcurrido
            tiempoTotal++; //Le añade a la variable tiempo el tiempo entre parada y parada
            //tiempoTotal += Time.deltaTime; //Le añade a la variable tiempo el tiempo entre frames
            //Actualizo la caja de texto del tiempo
            timer.text = formatTime(tiempoTotal);
            yield return new WaitForSeconds(1f);
        }
        
    }
    
    string formatTime(float time){

        //Formateo minutos y segundos a dos dígitos
        string minutos = Mathf.Floor(time / 60).ToString("00");
        string segundos = Mathf.Floor(time % 60).ToString("00");
    
        //Devuelvo el string formateado con : como separador
        return minutos + ":" + segundos;
  
    }
}
