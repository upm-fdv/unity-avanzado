using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivateMovement : MonoBehaviour
{

    public UnityEvent myEvent;

    private void OnCollisionEnter(Collision other)
    {
        if (myEvent == null)
        {
            print("F");
        }
        else
        {
            print("Evento activado: " + myEvent);
            myEvent.Invoke();
        }
    }
}
