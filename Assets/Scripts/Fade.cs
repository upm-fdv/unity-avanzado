using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{

    public GameObject image;

    private SpriteRenderer renderer;
    
    // Start is called before the first frame update
    void Start()
    {
        renderer = image.GetComponent<SpriteRenderer>();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("f"))
        {
            StartCoroutine(ChangeFade());
        }
    }
    
    IEnumerator ChangeFade()
    {
        Color c = renderer.material.color;
        for (float alpha = 1f; alpha >= 0; alpha -= 0.01f)
        {
            c.a = alpha;
            renderer.material.color = c;
            yield return new WaitForSeconds(.01f);
        }
    }
    
}
